package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.ListaEncadenada.Iterador;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		File archivo= new File(archivoPeliculas);
		if(archivo.exists())
		{
			try
			{
				FileReader reader = new FileReader(archivo);
				BufferedReader lector= new BufferedReader(reader);
				String linea= lector.readLine();
				while(linea!=null)
				{
					String[] parametrosPeliculas =linea.split(",");
					if(parametrosPeliculas.length<5)
					{    	
						String nombre=parametrosPeliculas[1];
						int anio = 0;
						if(nombre.contains("(")){
							String[] nombreYAnio = nombre.split("(");
							nombre = nombreYAnio[0];
							anio = Integer.valueOf(nombreYAnio[nombreYAnio.length-1]);
						}
						String genero=parametrosPeliculas[2];
						String[] generosPelicula = genero.split("|");
						ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
						for( int i = 0; i<generosPelicula.length;i++){
							String h = generosPelicula[i];
							listaGeneros.agregarElementoFinal(h);
						}
						VOPelicula pelicula = new VOPelicula(nombre,anio,listaGeneros);
						misPeliculas.agregarElementoFinal(pelicula);
					}
					linea= lector.readLine();
				}
				lector.close();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}


	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPelicula> x = new ListaEncadenada<VOPelicula>();
		Iterator iter = misPeliculas.iterator();
		while(iter.hasNext()){
			for(int i = 0; i<misPeliculas.darNumeroElementos();i++){
				if(misPeliculas.darElementoPosicionActual().getTitulo().substring(i, misPeliculas.darNumeroElementos()).equals(busqueda)){
					x.agregarElementoFinal(misPeliculas.darElementoPosicionActual());
				}
				else{
					for(int j = 0; j<misPeliculas.darNumeroElementos();j--){
						if(misPeliculas.darElementoPosicionActual().getTitulo().substring(misPeliculas.darNumeroElementos(),j ).equals(busqueda)){
							x.agregarElementoFinal(misPeliculas.darElementoPosicionActual());
						}
					}
				}
			}
			
		}
		return x;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}

}
